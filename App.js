import React, {Component} from 'react'
import { StyleSheet, Text, View, Button} from 'react-native'

export default class App extends Component{
    constructor(){
        super()
        this.state = {}
    }
    render(){
        return(
        <View style = {styles.container}>
            <View style = {styles.result}>
                <Text style = {styles.resultText}>11 * 11</Text>
            </View>
            <View style = {styles.calculation}>
                <Text style = {styles.calculationText}>121</Text>
            </View>
            <View style = {styles.buttons}>
                <View style = {styles.numbers}>
                    <View style = {styles.row}>
                        <Button title = "0"/>
                        <Button title = "0"/>
                        <Button title = "0"/>
                    </View>
                    <View style = {styles.row}>
                        <Button title = "0"/>
                        <Button title = "0"/>
                        <Button title = "0"/>
                    </View>
                    <View style = {styles.row}>
                        <Button title = "0"/>
                        <Button title = "0"/>
                        <Button title = "0"/>
                    </View>
                    <View style = {styles.row}>
                        <Button title = "0"/>
                        <Button title = "0"/>
                        <Button title = "0"/>
                    </View>
                </View>
                 <View style = {styles.column}>
                        <Button title = "+" color="#FF0000"/>
                        <Button title = "+" color="#FF0000"/>
                        <Button title = "+" color="#FF0000"/>
                        <Button title = "+" color="#FF0000"/>
                    </View>
            </View>
        </View>
        );
    }
}
const styles = StyleSheet.create(
    {

        container: {
            flex: 1
        },
        resultText: {
            fontSize: 24,
            color: 'white'
        },
        calculationText: {
            fontSize: 24,
            color: 'white'
        },
        row: {
            flexDirection: 'row',
            flex: 1,
            justifyContent: 'space-around',
            alignItems: 'center'
        },
        column: {
            Button:{
                width: 20
            },
            flexDirection: 'column',
            flex: 1,
            justifyContent: 'space-around',
            alignItems: 'center',
            backgroundColor: 'black',
        },
        result: {
            flex: 2,
            backgroundColor: 'red',
            justifyContent: 'center',
            alignItems: 'flex-end'
        },
        calculation: {
            flex: 1,
            backgroundColor: 'green',
            justifyContent: 'space-around',
            alignItems: 'flex-end'
        },
        buttons: {
            flex: 7,
            flexDirection: 'row'
        },
        numbers: {
            flex: 4,
            backgroundColor: 'yellow'
        }
    }
);